﻿using System;
using MenuSystem;
using BattleshipSystem;
using BattleshipConsoleUI;
using System.Collections.Generic;
using Domain.Menu;


namespace BattleshipConsoleApp
{
    
    /// <summary>
    /// Console Battleship Application Runtime.
    /// </summary>
    internal static class ConsoleApp
    {
        
        /// <summary>
        /// Access to BattleShip Logic.
        /// </summary>
        private static BattleShip _battleShip;
        
        
        /// <summary>
        /// Battleship Start Up.
        /// </summary>
        private static void Main()
        {
            var mainMenu = new Menu("Battleship 1.0 Main Menu", EMenuLevel.Root);
            
            mainMenu.AddMenuItems(new List<MenuItem>
            {
                new( "New Game", runGame)
            });

            new MenuLogic(mainMenu).Run();
        }

        
        /// <summary>
        /// Run the BattleShip Game.
        /// </summary>
        /// <returns></returns>
        private static string runGame()
        {
            var (size, playerAName, playerBName) = SettingsUI.SettingsInitialization();
            _battleShip = new BattleShip(playerAName, playerBName, size, size);
            
            //TODO! replace and fix switch turns to test.
            var num = 0;
            
            do
            {
                var (xCord, yCord, xSize, ySize, name) =
                                        PlaceUI.playerPlaceShip(_battleShip.GetPlayerBoard(true, false));
                
                var b1 = _battleShip.CreatePlayerShip(name, xSize, ySize);
                
                if (!b1)
                    Console.WriteLine("CAN'T CREATE.");
                else
                {
                    var b2 = _battleShip.PlacePlayerShip(num, xCord, yCord, false);

                    if (!b2)
                        Console.WriteLine("CAN'T LOCATE.");
                    else 
                        num++;
                }

            } while (true);
            
            do
            {
                var (xCoordinate, yCoordinate) = ShootUI.playerMakeMove(_battleShip.GetPlayerBoard(true, true));
                _battleShip.MakeMove(xCoordinate, yCoordinate);
                
            } while (true);

            return "";
        }
    }
}
