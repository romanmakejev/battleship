﻿using System.Linq;
using Domain.Battleship;
using System.Collections.Generic;


namespace BattleshipSystem
{

    /// <summary>
    /// Describes Battleship Game Logic.
    /// </summary>
    public class BattleShip
    {
        
        /// <summary>
        /// Describes Player A State.
        /// </summary>
        private readonly Player _playerA;
        
        
        /// <summary>
        /// Describes Player B State.
        /// </summary>
        private readonly Player _playerB;

        
        /// <summary>
        /// Describes Game Setting.
        /// </summary>
        private readonly Settings _settings;
        
        
        /// <summary>
        /// Basic BattleShip Game Constructor. Defines Players and Boards.
        /// </summary>
        /// <param name="nameA">NickName of the Player A.</param>
        /// <param name="nameB">NickName of the Player B.</param>
        /// <param name="boardSizeX">X Coordinate Size of the boards.</param>
        /// <param name="boardSizeY">Y Coordinate Size of the boards.</param>
        public BattleShip(string nameA, string nameB, int boardSizeX, int boardSizeY)
        {
            _playerA = new Player(nameA, true, boardSizeX, boardSizeY);
            _playerB = new Player(nameA, false, boardSizeX, boardSizeY);
        }
        
        
        /// <summary>
        /// Switch turns of Game Players.
        /// </summary>
        private void SwitchTurns()
        {
            new List<Player> { _playerA, _playerB}.ForEach(x => x.IsTurn = !x.IsTurn);
        }

        
        /// <summary>
        /// Create Ship for Currently making move Player.
        /// </summary>
        /// <param name="name">Name of the Ship to create.</param>
        /// <param name="xSize">X Coordinate Size of the Ship to create.</param>
        /// <param name="ySize">Y Coordinate Size of the Ship to create.</param>
        /// <returns>Indicator of Creation Success.</returns>
        public bool CreatePlayerShip(string name, int xSize, int ySize)
        {
            return DecideTurn()!.CreateShip(name, xSize, ySize);
        }


        /// <summary>
        /// Place Ship for Currently making move Player on the board.
        /// </summary>
        /// <param name="shipSerialNumber">Serial number of the ship to place.</param>
        /// <param name="xCoordinate">X Coordinate to place Ship.</param>
        /// <param name="yCoordinate">Y Coordinate to place Ship.</param>
        /// <param name="isRotate">Indicator of Ship rotation.</param>
        /// <returns>Indicator of CPlacement Success.</returns>
        public bool PlacePlayerShip(int shipSerialNumber, int xCoordinate, int yCoordinate, bool isRotate)
        {
            return DecideTurn()!.LocateShip(shipSerialNumber, xCoordinate,yCoordinate, isRotate);
        }

        
        /// <summary>
        /// Decide Whose turn currently is.
        /// </summary>
        /// <returns>Player Whose turn is.</returns>
        private Player DecideTurn()
        {
            return new List<Player> {_playerA, _playerB}.Find(x => x.IsTurn);
        }
        
        
        /// <summary>
        /// Currently making a move player places the bomb.
        /// </summary>
        /// <param name="xCoordinate">X Coordinate to place bomb.</param>
        /// <param name="yCoordinate">Y Coordinate to place bomb.</param>
        public void MakeMove(int xCoordinate, int yCoordinate)
        {
            var player = _playerA.IsTurn ? _playerA : _playerB;
            
            player.MakeMove(xCoordinate, yCoordinate);
            // SwitchTurns();
        }
        
        
        /// <summary>
        /// Get the needed copy of the needed players' board.
        /// </summary>
        /// <param name="isTurnPlayer">Needed players' board (Whose turn or not).</param>
        /// <param name="isFiring">Type of the needed board.</param>
        /// <returns>Deep Copy of the needed board.</returns>
        public BoardSquareState[,] GetPlayerBoard(bool isTurnPlayer, bool isFiring)
        {
            return new List<Player> {_playerA, _playerB}
                                .Single(x => x.IsTurn == isTurnPlayer).GetCopiedBoard(isFiring);
        }
    }
}