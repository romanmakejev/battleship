﻿using System;
using System.Linq;
using System.Collections.Generic;


namespace MenuSystem
{
    
    /// <summary>
    /// Menu Implementation.
    /// </summary>
    public class Menu
    {
        
        /// <summary>
        /// Show selected Menu Item in Menu Item List.
        /// </summary>
        private int _cursorPosition;
        
        
        /// <summary>
        /// String contain Menu Title.
        /// </summary>
        private readonly string _title;

        
        /// <summary>
        /// Indicator of this Menu Level.
        /// </summary>
        private readonly EMenuLevel _menuLevel;

        
        /// <summary>
        /// Number of Reserved Menu Items in list.
        /// </summary>
        private readonly int _reservedMenuItems;
        
        
        /// <summary>
        /// List stores Menu Items that belong to this Menu in order of addition.
        /// </summary>
        private readonly List<MenuItem> _menuItems = new();
        
        
        /// <summary>
        /// Reserved Menu Item "Main" initialization.
        /// </summary>
        private readonly MenuItem _menuItemMain = new("Main Menu");
        
        
        /// <summary>
        /// Reserved Menu Item "Return" initialization..
        /// </summary>
        private readonly MenuItem _menuItemReturn = new("Return");
        
        
        /// <summary>
        /// Reserved Menu Item "Exit" initialization..
        /// </summary>
        private readonly MenuItem _menuItemExit = new("Exit");
        
        
        /// <summary>
        /// Menu basic constructor.
        /// </summary>
        /// <param name="title">Title of the Menu.</param>
        /// <param name="menuLevel">Level of the current Menu.</param>
        public Menu(string title, EMenuLevel menuLevel)
        {
            
            _title = string.IsNullOrEmpty(title) ? "Menu" : title;
            _menuLevel = menuLevel;
            
            switch (_menuLevel)
            {
                case EMenuLevel.Root:
                    _menuItems.Add(_menuItemExit);
                    break;
                case EMenuLevel.First:
                    _menuItems.Add(_menuItemReturn);
                    _menuItems.Add(_menuItemExit);
                    break;
                case EMenuLevel.SecondOrMore:
                    _menuItems.Add(_menuItemReturn);
                    _menuItems.Add(_menuItemMain);
                    _menuItems.Add(_menuItemExit);
                    break;
            }

            _reservedMenuItems = _menuItems.Count;
        }
        
        
        /// <summary>
        /// Add Menu item to certain position in Menu list.
        /// </summary>
        /// <param name="item">Menu Item to be added.</param>
        /// <param name="position">Position in Menu list for addition.</param>
        private void AddMenuItem(MenuItem item, int position = -1)
        {
            // ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Local
            
            if (_menuItems.Any(x => x.Title == item.Title))
                throw new ApplicationException("Menu Item Already Exist in the list.");

            var addPosition = position == -1 ? _menuItems.Count - _reservedMenuItems : position;
            
            _menuItems.Insert(addPosition, item);
        }
        
        
        /// <summary>
        /// Add Menu Items from list to tne Menu list.
        /// </summary>
        /// <param name="items">List of Menu Items to be added.</param>
        public void AddMenuItems(List<MenuItem> items)
        {
            items.ForEach(x => AddMenuItem(x));
        }


        /// <summary>
        /// Menu execution.
        /// </summary>
        /// <returns>User choice of Menu Item.</returns>
        public string Run(string calculationData = null)
        {
            bool isDone;
            string executionResult;

            do
            {
                OutputMenu(calculationData);
                (isDone, executionResult) = KeyboardControl();
                
                // Special Menu Items Methods. TODO?
                
                if (executionResult == _menuItemMain.Title && _menuLevel != EMenuLevel.Root) return _menuItemMain.Title;

                if (executionResult == _menuItemExit.Title) return _menuItemExit.Title;

            } while (!isDone);
            
            return executionResult;
        }

        
        /// <summary>
        /// Activate keyboard control for the Menu.
        /// </summary>
        /// <returns>
        /// Tuple of two elements: first one indicates if Menu is done and second one indicates the results.
        /// </returns>
        private (bool, string) KeyboardControl()
        {
            var isDone = false;
            var executionResult = "";
            
            switch (Console.ReadKey(true).Key)
            {
                case ConsoleKey.UpArrow:
                    _cursorPosition = _cursorPosition > 0 ? _cursorPosition - 1 : 0;
                    break;
                case ConsoleKey.DownArrow:
                    _cursorPosition = _cursorPosition < _menuItems.Count - 1 ? _cursorPosition + 1 : _cursorPosition;
                    break;
                case ConsoleKey.Enter:
                    isDone = _menuItems.GetRange(_menuItems.Count - _reservedMenuItems, _reservedMenuItems)
                                       .Any(x => x == _menuItems[_cursorPosition]);

                    executionResult = !isDone ? _menuItems[_cursorPosition].ExecuteMethod() : _menuItems[_cursorPosition].Title;
                    break;
            }
            
            return (isDone, executionResult);
        }
        
        
        /// <summary>
        /// Display Menu in Console.
        /// </summary>
        private void OutputMenu(string calculationData = null)
        {
            Console.Clear();
            
            var menuTitle = $"====>{_title}<====";
            
            DrawCenteredText(menuTitle, 0);

            if (calculationData != null)
            {
                DrawCenteredText(string.Concat(Enumerable.Repeat("=", menuTitle.Length)), Console.CursorTop);
                DrawCenteredText(string.Concat(Enumerable.Repeat("-", menuTitle.Length)), Console.CursorTop + 2);
                DrawCenteredText("Current value => " + calculationData, Console.CursorTop);
                DrawCenteredText(string.Concat(Enumerable.Repeat("-", menuTitle.Length)), Console.CursorTop);
            }

            Console.CursorTop += 2;

            foreach (var item in _menuItems)
            {
                Console.ForegroundColor = _cursorPosition == _menuItems.IndexOf(item) ? ConsoleColor.Blue
                    : Console.ForegroundColor;
                
                DrawCenteredText(item.Title, Console.CursorTop);
                
                Console.ResetColor();
            }
            
            DrawCenteredText(string.Concat(Enumerable.Repeat("=", menuTitle.Length)), Console.CursorTop + 2);
        }


        /// <summary>
        /// Draw text in the middle of the console.
        /// </summary>
        /// <param name="textToDraw"></param>
        /// <param name="xPos">Position on X coordinate for text.</param>
        private static void DrawCenteredText(string textToDraw, int xPos)
        {
            Console.SetCursorPosition((Console.WindowWidth - textToDraw.Length) / 2 , xPos);
            Console.WriteLine(textToDraw);
        }
    }
}