﻿

namespace MenuSystem
{
    
    /// <summary>
    /// Enumeration of possible Menu Levels.
    /// </summary>
    public enum EMenuLevel
    {
        Root,
        First,
        SecondOrMore
    }
}