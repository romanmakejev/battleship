﻿using System;


namespace MenuSystem
{
    /// <summary>
    /// Implementation of Menu Item with all necessary data.
    /// </summary>
    public class MenuItem
    {
        
        /// <summary>
        /// String contains title of the menu item.
        /// </summary>
        public string Title { get; }
        
        
        /// <summary>
        /// Func contains method to be executed when menu item is triggered.
        /// </summary>
        public Func<string> ExecuteMethod { get; }

        
        /// <summary>
        /// Basic constructor for Menu Item.
        /// </summary>
        /// <param name="title">Title of the Menu Item.</param>
        /// <param name="executeMethod">Method that should be executed when Menu Item is triggered.</param>
        public MenuItem(string title, Func<string> executeMethod = null)
        {
            
            if (string.IsNullOrEmpty(title)) throw new ArgumentException("Title cannot be empty.");
            
            Title = title;
            ExecuteMethod = executeMethod;
        }

        
        /// <summary>
        /// String implementation of Menu Item.
        /// </summary>
        /// <returns>Menu Item Title.</returns>
        public override string ToString()
        {
            return Title;
        }
    }
}