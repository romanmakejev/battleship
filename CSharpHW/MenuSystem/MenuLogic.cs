﻿using Domain.Menu;
using MenuConsoleUI;


namespace MenuSystem
{
    
    public class MenuLogic
    {

        public Menu Menu;

        
        public MenuLogic(Menu menu)
        {
            Menu = menu;
        }
        
        
        /// <summary>
        /// Menu execution.
        /// </summary>
        /// <returns>User choice of Menu Item.</returns>
        public string Run()
        {
            bool isDone;
            string executionResult;

            do
            {
                MenuUI.DisplayMenu(Menu);
                (isDone, executionResult) = MenuUI.KeyboardControl(Menu);

                if (executionResult == Menu.MenuItemMain.Title && Menu.MenuLevel != EMenuLevel.Root) return Menu.MenuItemMain.Title;

                if (executionResult == Menu.MenuItemExit.Title) return Menu.MenuItemExit.Title;

            } while (!isDone);
            
            return executionResult;
        }
    }
}