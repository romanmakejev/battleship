﻿using System;
using System.Threading;


namespace CalculatorSystem
{
    
    /// <summary>
    /// Class describes Calculator Functionality.
    /// </summary>
    public class Calculator
    {

        /// <summary>
        /// Calculation value.
        /// </summary>
        public double CalculatorCurrentDisplay;


        /// <summary>
        /// Write text in the middle of Console.
        /// </summary>
        /// <param name="text">Text to be written.</param>
        /// <param name="topPosition">X coordinate position for cursor.</param>
        private static void ConsoleCursorWrite(string text, int topPosition)
        {
            Console.SetCursorPosition((Console.WindowWidth - text.Length) / 2, topPosition);
            Console.Write(text);
        }
        
        
        /// <summary>
        /// Displays the calculation process and parses User Input.
        /// </summary>
        /// <param name="sign">String of calculation sign.</param>
        /// <returns>Converted User Input.</returns>
        private double CalculationDisplayProcess(string sign)
        {
            ConsoleCursorWrite(
                sign == "new" ? "Current value => " : $"Current value => {CalculatorCurrentDisplay} {sign} ", 5);
            
            // User input parsing and validation.
            if (double.TryParse(Console.ReadLine()?.Trim(), out var number)) return number;
     
            ErrorMessage("Input should be a number.");
            return 0;
        }

        
        /// <summary>
        /// Prints Error message on screen if calculation isn't possible.
        /// </summary>
        /// <param name="errorMessage">Message to be displayed on screen.</param>
        private static void ErrorMessage(string errorMessage)
        {
            Console.Clear();
            ConsoleCursorWrite(errorMessage, (Console.WindowHeight - 1) / 2);

            Thread.Sleep(2000);
        }
        

        /// <summary>
        /// Adds number to current calculation.
        /// </summary>
        /// <returns>Empty String if calculation Succeeded.</returns>
        public string Add()
        {
            CalculatorCurrentDisplay += CalculationDisplayProcess("+");
            return "";
        }
        

        /// <summary>
        /// Subtract number from current calculation.
        /// </summary>
        /// <returns>Empty String if calculation Succeeded.</returns>
        public string Subtract()
        {
            CalculatorCurrentDisplay -= CalculationDisplayProcess("-");
            return "";
        }

        
        /// <summary>
        /// Divide current calculation on number.
        /// </summary>
        /// <returns>Empty String if calculation Succeeded.</returns>
        public string Divide()
        {
            var userInput = CalculationDisplayProcess("/");

            if (userInput == 0) 
                ErrorMessage("Cannot divide on 0.");
            else 
                CalculatorCurrentDisplay /= userInput;

            return "";
        }
        
        
        /// <summary>
        /// Multiply current calculation on number..
        /// </summary>
        /// <returns>Empty String if calculation Succeeded.</returns>
        public string Multiply()
        {
            CalculatorCurrentDisplay *= CalculationDisplayProcess("*");
            return "";
        }
        
        
        /// <summary>
        /// Elevate current calculation on number.
        /// </summary>
        /// <returns>Empty String if calculation Succeeded.</returns>
        public string Elevate()
        {
            CalculatorCurrentDisplay = Math.Pow(CalculatorCurrentDisplay, CalculationDisplayProcess("^"));
            return "";
        }
        
        
        /// <summary>
        /// Negate number current calculation.
        /// </summary>
        /// <returns>Empty String if calculation Succeeded.</returns>
        public string Negate()
        {
            CalculatorCurrentDisplay = -CalculatorCurrentDisplay;
            return "";
        }
        
        
        /// <summary>
        /// Square root of the current calculation.
        /// </summary>
        /// <returns>Empty String if calculation Succeeded.</returns>
        public string Root()
        {
            if (CalculatorCurrentDisplay < 0)
                ErrorMessage("Cannot find root of a negative number.");
            else
                CalculatorCurrentDisplay = Math.Sqrt(CalculatorCurrentDisplay);
            
            return "";
        }


        /// <summary>
        /// Square of current calculation.
        /// </summary>
        /// <returns>Empty String if calculation Succeeded.</returns>
        public string Square()
        {
            CalculatorCurrentDisplay = Math.Pow(CalculatorCurrentDisplay, 2);
            return "";
        }
        
        
        /// <summary>
        /// Abs of current calculation.
        /// </summary>
        /// <returns>Empty String if calculation Succeeded.</returns>
        public string Abs()
        {
            CalculatorCurrentDisplay = Math.Abs(CalculatorCurrentDisplay);
            return "";
        }

        
        /// <summary>
        /// Set new initial value for calculator.
        /// </summary>
        /// <returns>Empty String if calculation Succeeded.</returns>
        public string InitialValue()
        {
            CalculatorCurrentDisplay = CalculationDisplayProcess("new");
            return "";
        }
        
        
        /// <summary>
        /// Remove the value of calculator to zero.
        /// </summary>
        /// <returns>Empty String if calculation Succeeded.</returns>
        public string Erase()
        {
            CalculatorCurrentDisplay = 0;
            return "";
        }
    }
}