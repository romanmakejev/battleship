﻿

namespace Domain.Battleship
{
    
    /// <summary>
    /// Represents Ship state.
    /// </summary>
    public class Ship
    {
        
        /// <summary>
        /// Name of the Ship.
        /// </summary>
        private readonly string Name;
    
        
        /// <summary>
        /// Ships' Serial Number.
        /// </summary>
        public readonly int ShipNumber;

        
        /// <summary>
        /// Ship X Coordinate Lenght.
        /// </summary>
        private int XSize;

        
        /// <summary>
        /// Ship Y Coordinate Lenght.
        /// </summary>
        private int YSize;

        
        /// <summary>
        /// Basic Ship Constructor. Initializing Name, Sizes and SerialNumber.
        /// </summary>
        /// <param name="name">Name of the current ship.</param>
        /// <param name="shipNumber">Serial number of the ship.</param>
        /// <param name="xSize">Ship X Coordinate Lenght.</param>
        /// <param name="ySize">Ship Y Coordinate Lenght.</param>
        public Ship(string name, int shipNumber, int xSize, int ySize)
        {
            Name = name;
            ShipNumber = shipNumber;
            XSize = xSize;
            YSize = ySize;
        }

        
        /// <summary>
        /// Rotate Ship on Changing X and Y sizes.
        /// </summary>
        public void RotateShip()
        {
            var xSize = XSize;

            XSize = YSize;
            YSize = xSize;
        }

        
        /// <summary>
        /// Get sizes of the Ship on X and Y coordinate.
        /// </summary>
        /// <returns>Sizes of the Ship.</returns>
        public (int, int) GetSizes() => (XSize, YSize);
        
        
        /// <summary>
        /// String representation of the Ship.
        /// </summary>
        public override string ToString() => Name;
    }
}