﻿

namespace Domain.Battleship
{
    
    /// <summary>
    /// Represents BattleShip Game Settings' state.
    /// </summary>
    public class Settings
    {
        
        /// <summary>
        /// Represents the X Coordinate Lenght of the Board.
        /// </summary>
        private int xCoordinateSize { get; set; }
        
        
        /// <summary>
        /// Represents the Y Coordinate Lenght of the Board.
        /// </summary>
        private int yCoordinateSize { get; set; }
        
        
        /// <summary>
        /// Represents the state of difference of Users' Ships.
        /// </summary>
        private bool isDifferentShips { get; set; }
        
        
        /// <summary>
        /// Represents the state of Users' Ships corner touching.
        /// </summary>
        private bool canCornersTouch { get; set; }
        
        
        /// <summary>
        /// Represents the state of Users' Ships side touching.
        /// </summary>
        private bool canSidesTouch { get; set; }
        
        
        /// <summary>
        /// Represents if game is in single player mode.
        /// </summary>
        private bool isSinglePlayer { get; set; }
    }
}