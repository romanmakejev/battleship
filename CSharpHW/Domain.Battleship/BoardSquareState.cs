﻿

namespace Domain.Battleship
{
    
    /// <summary>
    /// Represents state of single board cell.
    /// </summary>
    public struct BoardSquareState
    {

        /// <summary>
        /// Indicates if Ship is located on the cell.
        /// </summary>
        public bool IsShip { get; set; }
        

        /// <summary>
        /// Indicates if Bomb is located on the cell.
        /// </summary>
        public bool IsBomb { get; set; }


        /// <summary>
        /// Indicates if Ship is located on the cell it's serial number.
        /// </summary>
        public int ShipSerialNumber { get; set; } 

        
        /// <summary>
        /// String representation of the single board cell.
        /// </summary>
        /// <returns>String representation of current cell.</returns>
        public override string ToString()
        {
            return (IsShip, IsBomb) switch
            {
                (false, false) => " ",
                (false, true) => "*",
                (true, false) => "0",
                (true, true) => " X"
            };
        }
    }
}