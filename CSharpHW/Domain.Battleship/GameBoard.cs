﻿

namespace Domain.Battleship
{
    
    /// <summary>
    /// Represents state of the Game Board.
    /// </summary>
    public class GameBoard
    {

        /// <summary>
        /// Indicates The Boards' maximum Ship capacity.
        /// </summary>
        private int boardShipCapacity;
        
        
        /// <summary>
        /// Indicates The Board state.
        /// </summary>
        private readonly BoardSquareState[,] Board;

        
        /// <summary>
        /// Basic Game Board Constructor. Defines board sizes.
        /// </summary>
        /// <param name="xSize">X coordinate size for board.</param>
        /// <param name="ySize">Y coordinate size for board.</param>
        public GameBoard(int xSize, int ySize)
        {
            Board = new BoardSquareState[xSize, ySize];
            boardShipCapacity = xSize * ySize / 5;
        }

        
        /// <summary>
        /// Places the bomb on the board.
        /// </summary>
        /// <param name="xCoordinate">X Coordinate to place bomb.</param>
        /// <param name="yCoordinate">Y Coordinate to place bomb.</param>
        public void MakeMove(int xCoordinate, int yCoordinate)
        {
            Board[xCoordinate, yCoordinate].IsBomb = true;
        }


        /// <summary>
        /// Place Ship on the board.
        /// </summary>
        /// <param name="ship">Ship to place.</param>
        /// <param name="xCoordinate">X Coordinate to place Ship.</param>
        /// <param name="yCoordinate">Y Coordinate to place Ship.</param>
        public void LocateShip(Ship ship, int xCoordinate, int yCoordinate)
        {
            var (xSize, ySize) = ship.GetSizes();

            for (var y = yCoordinate; y < yCoordinate + ySize; y++)
            {
                for (var x = xCoordinate; x < xCoordinate + xSize; x++)
                {
                    Board[x, y].IsShip = true;
                    Board[x, y].ShipSerialNumber = ship.ShipNumber;
                }   
            }

            boardShipCapacity -= xSize * ySize;
        }
        
        
        /// <summary>
        /// Get the Copy of the needed board.
        /// </summary>
        /// <returns>Deep Copy of the needed board.</returns>
        public BoardSquareState[,] CopyBoard()
        {
            
            var res = new BoardSquareState[Board.GetLength(0),Board.GetLength(1)];
            
            for (var y = 0; y < Board.GetLength(0); y++)
            {
                for (var x = 0; x < Board.GetLength(1); x++)
                {
                    res[x, y] = Board[x, y];
                }
            }

            return res;
        }


        /// <summary>
        /// Calculates fitting of the Given Ship on the board.
        /// </summary>
        /// <param name="xSize">X Coordinate Size of Ship.</param>
        /// <param name="ySize">Y Coordinate Size of Ship.</param>
        /// <returns>Indicator of fitting.</returns>
        public bool CanShipFit(int xSize, int ySize) =>
            
            xSize * ySize <= boardShipCapacity && xSize < Board.GetLength(1) && ySize < Board.GetLength(0);


        /// <summary>
        /// Calculates if Ship Can be placed on the board in the given location.
        /// </summary>
        /// <param name="ship">Ship to place.</param>
        /// <param name="xCoordinate">X Coordinate for Ship placement.</param>
        /// <param name="yCoordinate">Y Coordinate for Ship placement.</param>
        /// <returns>Indicator of placing.</returns>
        public bool CanShipBeLocated(Ship ship, int xCoordinate, int yCoordinate)
        {
            var (xSize, ySize) = ship.GetSizes();

            for (var y = yCoordinate; y < yCoordinate + ySize; y++)
            {
                for (var x = xCoordinate; x < xCoordinate + xSize; x++)
                {
                    if (Board[x, y].IsShip) return false;
                }   
            }

            return true;
        }
    }
}