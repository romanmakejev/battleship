﻿using System.Collections.Generic;


namespace Domain.Battleship
{
    
    /// <summary>
    /// Represents Players' state.
    /// </summary>
    public class Player
    {

        /// <summary>
        /// Nickname of the Player.
        /// </summary>
        private readonly string Name;
        

        /// <summary>
        /// Indicates if User should make a move.
        /// </summary>
        public bool IsTurn { get; set; }
        
        
        /// <summary>
        /// Indicates Firing Board state.
        /// </summary>
        private readonly GameBoard FiringBoard;


        /// <summary>
        /// Indicates Players' Own Board state.
        /// </summary>
        private readonly GameBoard OwnBoard;

        
        /// <summary>
        /// Indicates Players' Own Ships.
        /// </summary>
        private readonly List<Ship> OwnShips = new();

        
        /// <summary>
        /// Basic Player Constructor. Initializing Name and Board sizes.
        /// </summary>
        /// <param name="name">Nickname of the Player.</param>
        /// <param name="isTurn">Should User make a move indicator.</param>
        /// <param name="xSize">X coordinate size for board.</param>
        /// <param name="ySize">Y coordinate size for board.</param>
        public Player(string name, bool isTurn, int xSize, int ySize)
        {
            Name = name;
            IsTurn = isTurn;
            FiringBoard = new GameBoard(xSize, ySize);
            OwnBoard = new GameBoard(xSize, ySize);
        }

        
        /// <summary>
        /// Places the bomb on Players' the board.
        /// </summary>
        /// <param name="xCoordinate">X Coordinate to place bomb.</param>
        /// <param name="yCoordinate">Y Coordinate to place bomb.</param>
        public void MakeMove(int xCoordinate, int yCoordinate)
        {
            FiringBoard.MakeMove(xCoordinate, yCoordinate);
        }

        
        /// <summary>
        /// Create Ship for Player.
        /// </summary>
        /// <param name="name">Name of the Ship to create.</param>
        /// <param name="xSize">X Coordinate Size of the Ship to create.</param>
        /// <param name="ySize">Y Coordinate Size of the Ship to create.</param>
        /// <returns>Indicator of creation success.</returns>
        public bool CreateShip(string name, int xSize, int ySize)
        {
            var isCreated = OwnBoard.CanShipFit(xSize, ySize);

            if (isCreated) OwnShips.Add(new Ship(name, OwnShips.Count, xSize, ySize));

            return isCreated;
        }


        /// <summary>
        /// Place All Ships on Players' own board.
        /// </summary>
        /// <param name="serialShipNumber">Serial number of the ship to place.</param>
        /// <param name="xCoordinate">X Coordinate to place Ship.</param>
        /// <param name="yCoordinate">Y Coordinate to place Ship.</param>
        /// /// <param name="isRotate">Indicator of Ship rotation.</param>
        /// <returns>Indicator of location success.</returns>
        public bool LocateShip(int serialShipNumber, int xCoordinate, int yCoordinate, bool isRotate)
        {
            var shipToLocate = OwnShips.Find(x => x.ShipNumber == serialShipNumber);

            if (isRotate) shipToLocate!.RotateShip();
            
            var canBeLocated = OwnBoard.CanShipBeLocated(shipToLocate, xCoordinate, yCoordinate);

            if (canBeLocated) OwnBoard.LocateShip(shipToLocate, xCoordinate, yCoordinate);

            return canBeLocated;
        }
        
        
        /// <summary>
        /// Get the needed board instance.
        /// </summary>
        /// <param name="isFiring">Defines which board is needed.</param>
        /// <returns></returns>
        public BoardSquareState[,] GetCopiedBoard(bool isFiring)
        {
            return isFiring ? FiringBoard.CopyBoard() : OwnBoard.CopyBoard();
        }
        
        
        /// <summary>
        /// String representation of the Player.
        /// </summary>
        public override string ToString() => Name;
    }
}