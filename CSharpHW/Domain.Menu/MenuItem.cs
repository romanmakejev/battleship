﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading;


namespace Domain.Menu
{
    
    /// <summary>
    /// Implementation of Menu Item with all necessary data.
    /// </summary>
    public class MenuItem
    {
        
        /// <summary>
        /// Show selected Menu Item Option in Menu Item Option List.
        /// </summary>
        public int YCursorPosition { get; private set; }
        
        
        /// <summary>
        /// String contains title of the menu item.
        /// </summary>
        public string Title { get; }
        
        
        /// <summary>
        /// Func contains method to be executed when Menu Item is triggered.
        /// </summary>
        public Func<string> ExecuteMethod { get; }


        /// <summary>
        /// List of Menu Item Options belonging to current Menu Item.
        /// </summary>
        private readonly List<MenuItemOption> MenuItemOptions = new();
        
        
        /// <summary>
        /// Basic constructor for Menu Item.
        /// </summary>
        /// <param name="title">Title of the Menu Item.</param>
        /// <param name="executeMethod">Method that should be executed when Menu Item is triggered.</param>
        public MenuItem(string title, Func<string> executeMethod = null)
        {
            
            if (string.IsNullOrEmpty(title)) throw new ArgumentException("Title cannot be empty.");
            
            Title = title;
            ExecuteMethod = executeMethod;
        }
        
        
        /// <summary>
        /// Add Menu Item Option to certain position in Menu Item Option list.
        /// </summary>
        /// <param name="item">Menu Item Option to be added.</param>
        /// <param name="position">Position in Menu list for addition.</param>
        private void AddMenuItemOption(MenuItemOption item, int position = -1)
        {

            if (MenuItemOptions.Any(x => x.Title == item.Title))
                throw new ApplicationException("Menu Item Option Already Exist in the list.");

            var insertPosition = position == -1 ? MenuItemOptions.Count : position;
            
            MenuItemOptions.Insert(insertPosition, item);
        }
        
        
        /// <summary>
        /// Add Menu Item Options from list to tne Menu Item option list.
        /// </summary>
        /// <param name="items">List of Menu Item options to be added.</param>
        public void AddMenuItemOptions(List<MenuItemOption> items) => items.ForEach(x => AddMenuItemOption(x));

        
        /// <summary>
        /// Return Copy of the List of Menu Item Options.
        /// </summary>
        /// <returns>Copy of the List of Menu Item Options</returns>
        public List<MenuItemOption> CopyOfMenuItemOptions() => MenuItemOptions.ConvertAll(x => x.Clone());

        
        /// <summary>
        /// Move Cursor Position from current Position. Between Menu Items and Menu Items Options.
        /// </summary>
        /// <param name="yCursorPosition">Y CoordinatePosition to move cursor.</param>
        public void MoveCursorPosition(int yCursorPosition)
        {
            if (yCursorPosition < 0 || yCursorPosition > MenuItemOptions.Count)
                throw new ArgumentException("Can't Move Outside the Menu Item Options!");
            
            YCursorPosition = yCursorPosition;
        }
        

        /// <summary>
        /// Return the deep Copy of Menu Item.
        /// </summary>
        /// <returns>Return the deep Copy of Menu Item.</returns>
        public MenuItem Clone()
        {
            var clone = new MenuItem(Title, ExecuteMethod) {YCursorPosition = YCursorPosition};
            clone.AddMenuItemOptions(CopyOfMenuItemOptions());

            return clone;
        }
   
        
        /// <summary>
        /// String implementation of Menu Item.
        /// </summary>
        /// <returns>Menu Item Title.</returns>
        public override string ToString() => Title;
    }
}