﻿using System;
using System.Linq;
using System.Collections.Generic;


namespace Domain.Menu
{
    
    /// <summary>
    /// Menu Implementation.
    /// </summary>
    public class Menu
    {

        /// <summary>
        /// Show selected Menu Item in Menu Item List.
        /// </summary>
        public int XCursorPosition { get; private set; }


        /// <summary>
        /// String contain Menu Title.
        /// </summary>
        public string Title { get; }


        /// <summary>
        /// Indicator of this Menu Level.
        /// </summary>
        public EMenuLevel MenuLevel { get; }


        /// <summary>
        /// Number of Reserved Menu Items in list.
        /// </summary>
        public int ReservedMenuItems { get; }


        /// <summary>
        /// List stores Menu Items that belong to this Menu in order of addition.
        /// </summary>
        private readonly List<MenuItem> MenuItems = new();
        
        
        /// <summary>
        /// Reserved Menu Item "Main" initialization.
        /// </summary>
        public readonly MenuItem MenuItemMain = new("Main Menu");
        
        
        /// <summary>
        /// Reserved Menu Item "Return" initialization..
        /// </summary>
        public readonly MenuItem MenuItemReturn = new("Return");
        
        
        /// <summary>
        /// Reserved Menu Item "Exit" initialization..
        /// </summary>
        public readonly MenuItem MenuItemExit = new("Exit");
        
        
        /// <summary>
        /// Menu basic constructor.
        /// </summary>
        /// <param name="title">Title of the Menu.</param>
        /// <param name="menuLevel">Level of the current Menu.</param>
        public Menu(string title, EMenuLevel menuLevel)
        {
            
            Title = string.IsNullOrEmpty(title) ? "Menu" : title;
            MenuLevel = menuLevel;

            ReservedMenuItemFiller(menuLevel);
            ReservedMenuItems = MenuItems.Count;
        }

        
        /// <summary>
        /// Fill Menu Items list with Reserved Menu items based on Menu Level.
        /// </summary>
        /// <param name="menuLevel">Current Menu level.</param>
        private void ReservedMenuItemFiller(EMenuLevel menuLevel)
        {
            switch (menuLevel)
            {
                case EMenuLevel.Root:
                    MenuItems.Add(MenuItemExit);
                    break;
                case EMenuLevel.First:
                    MenuItems.Add(MenuItemReturn);
                    MenuItems.Add(MenuItemExit);
                    break;
                case EMenuLevel.SecondOrMore:
                    MenuItems.Add(MenuItemReturn);
                    MenuItems.Add(MenuItemMain);
                    MenuItems.Add(MenuItemExit);
                    break;
            }
        }
        
        
        /// <summary>
        /// Add Menu item to certain position in Menu list.
        /// </summary>
        /// <param name="item">Menu Item to be added.</param>
        /// <param name="position">Position in Menu list for addition.</param>
        private void AddMenuItem(MenuItem item, int position = -1)
        {
            if (MenuItems.Any(x => x.Title == item.Title))
                throw new ApplicationException("Menu Item Already Exist in the list.");

            var addPosition = position == -1 ? MenuItems.Count - ReservedMenuItems : position;
            
            MenuItems.Insert(addPosition, item);
        }
        
        
        /// <summary>
        /// Add Menu Items from list to tne Menu list.
        /// </summary>
        /// <param name="items">List of Menu Items to be added.</param>
        public void AddMenuItems(List<MenuItem> items) => items.ForEach(x => AddMenuItem(x));


        /// <summary>
        /// Move Cursor Position from current Position. Between Menu Items and Menu Items Options.
        /// </summary>
        /// <param name="cursorPosition">Position to move cursor.</param>
        public void MoveCursorPosition((int, int) cursorPosition)
        {
            var (xPos, yPos) = cursorPosition;

            if (xPos < 0 || xPos > MenuItems.Count - 1)
                throw new ArgumentException("Can't Move Outside the Menu Items!");

            MenuItems[xPos].MoveCursorPosition(yPos);
            
            XCursorPosition = xPos;
        }


        /// <summary>
        /// Return the deep Copy of Menu Items list.
        /// </summary>
        /// <returns>Return the deep Copy of Menu Items list.</returns>
        public List<MenuItem> CopyOfMenuItems() => MenuItems.ConvertAll(x => x.Clone());


        /// <summary>
        /// String implementation of Menu.
        /// </summary>
        /// <returns></returns>
        public override string ToString() => Title;
        
    }
}