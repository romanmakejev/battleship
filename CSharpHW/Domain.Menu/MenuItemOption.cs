﻿using System;


namespace Domain.Menu
{
    
    /// <summary>
    /// Implementation of Menu Item Option with all necessary data.
    /// </summary>
    public class MenuItemOption
    {
        
        /// <summary>
        /// String contains Title of the Menu Item option.
        /// </summary>
        public string Title { get; }
        
        
        /// <summary>
        /// Func contains method to be executed when Menu Item option is triggered.
        /// </summary>
        private Func<string> ExecuteMethod { get; }


        /// <summary>
        /// Basic constructor for Menu Item option.
        /// </summary>
        /// <param name="title">Title of the Menu Item option.</param>
        /// <param name="executeMethod">Method that should be executed when Menu Item option is triggered.</param>
        public MenuItemOption(string title, Func<string> executeMethod = null)
        {
            if (string.IsNullOrEmpty(title)) throw new ArgumentException("Title cannot be empty.");
            
            Title = title;
            ExecuteMethod = executeMethod;
        }


        /// <summary>
        /// Return the deep Copy of Menu Item Option.
        /// </summary>
        /// <returns>Return the deep Copy of Menu Item Option.</returns>
        public MenuItemOption Clone() => new MenuItemOption(Title, ExecuteMethod);
        
          
        /// <summary>
        /// String implementation of Menu Item option.
        /// </summary>
        /// <returns>Menu Item option Title.</returns>
        public override string ToString() => Title;
        
    }
}