﻿

namespace Domain.Menu
{
    
    /// <summary>
    /// Enumeration of possible Menu Levels.
    /// </summary>
    public enum EMenuLevel
    {
        Undefined,
        Root,
        First,
        SecondOrMore
    }
}