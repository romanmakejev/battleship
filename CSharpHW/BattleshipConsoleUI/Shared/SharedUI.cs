﻿using System;
using System.Linq;


namespace BattleshipConsoleUI
{
    
    /// <summary>
    /// Describes Shared Functionality for Battleship Game Solution.
    /// </summary>
    public static class SharedUI
    {

        /// <summary>
        /// Draw Header of the Game.
        /// </summary>
        /// <returns>Header ending X Coordinate position relative to the Console.</returns>
        public static int GameHeader()
        {
            DrawCenteredText("--->BattleShip Game 1.0<---", -1, 1, true);
            DrawCenteredText(StringRepeater("=", Console.WindowWidth), -1, Console.CursorTop + 1, true);

            return Console.CursorTop - 1;
        }


        /// <summary>
        /// Draw the boarders of the Game.
        /// </summary>
        /// <param name="startPosition">Starting X Coordinate position for drawing boarders.</param>
        /// <param name="endPosition">Ending X Coordinate position for drawing boarders.</param>
        public static void GameBoarders(int startPosition, int endPosition)
        {
            for (var x = startPosition; x < endPosition; x++)
            {
                SymmetricalDrawer(x, 5, "<>");
            }
        }
        
        
        /// <summary>
        /// Draw the footer of the Game.
        /// </summary>
        /// <param name="startPosition">Starting X Coordinate position for drawing footer.</param>
        public static void GameFooter(int startPosition)
        {
            DrawCenteredText(StringRepeater("=", Console.WindowWidth), -1, startPosition, true);
        }


        /// <summary>
        /// Remove the scroll bar from the console application.
        /// </summary>
        public static void ScrollBarRemover()
        {
            Console.SetWindowSize(Console.WindowWidth, Console.WindowHeight);
            Console.SetBufferSize(Console.WindowWidth, Console.WindowHeight);
        }
        

        /// <summary>
        /// Draw Symmetrically on both sides of the Console.
        /// </summary>
        /// <param name="xIndent">Indent from the top side of the Console.</param>
        /// <param name="yIndent">Indent from the left side of the Console.</param>
        /// <param name="toWrite">Text to be written symmetrically.</param>
        private static void SymmetricalDrawer(int xIndent, int yIndent, string toWrite)
        {
            Console.SetCursorPosition(yIndent, xIndent);
            Console.Write(toWrite);
            Console.SetCursorPosition(Console.WindowWidth - yIndent - 1, xIndent);
            Console.Write(toWrite);
        }
        
        
        /// <summary>
        /// Repeat the provided text for certain amount of times.
        /// </summary>
        /// <param name="repeatText">Text to be repeated.</param>
        /// <param name="repeatCount">Amount of repetitions.</param>
        /// <returns>Repeated text for certain amount of times.</returns>
        public static string StringRepeater(string repeatText, int repeatCount)
        {
            return string.Concat(Enumerable.Repeat(repeatText, repeatCount));
        }
        
        
        /// <summary>
        /// Draw text in the middle of the console.
        /// </summary>
        /// <param name="textToDraw">String to Draw.</param>
        /// <param name="lenght">Complete Length of the string.</param>
        /// <param name="xPos">Position on X coordinate for text.</param>
        /// <param name="IsNew">Indicator of new line start.</param>
        public static void DrawCenteredText(string textToDraw, int lenght, int xPos, bool IsNew)
        {
            var textLenght = lenght == -1 ? textToDraw.Length : lenght;
            
            Console.SetCursorPosition((Console.WindowWidth - textLenght) / 2 , xPos);
            
            if (IsNew) Console.WriteLine(textToDraw);
            
            else Console.Write(textToDraw);
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        /// <summary>
        /// Validate User Input on Errors.
        /// </summary>
        /// <param name="message">Message to be displayed for hint.</param>
        /// <param name="xPos">Position on X coordinate for text.</param>
        /// <param name="isNumber">Check if input should be numeric.</param>
        /// <returns></returns>
        public static string InputValidation(string message, int xPos, bool isNumber)
        {
            
            bool isSuccess;
            string inputResult;
            
            do
            {
                DrawCenteredText(message,-1, xPos, false);
                inputResult = Console.ReadLine()?.Trim();
                isSuccess = !isNumber || int.TryParse(inputResult, out _);

            } while (!isSuccess);
            
            return inputResult;
        }

        
        

        
        /// <summary>
        /// Allow User to use navigation system.
        /// </summary>
        /// <param name="cursorPosition">Current cursor position indicator.</param>
        /// <param name="limitations">Border indicators.</param>
        /// <returns>Current cursor position and Success Indicator.</returns>
        public static (int, int, bool) keyboardControl((int, int) cursorPosition, (int, int) limitations)
        {
            
            // Unpacking Tuples
            var (xBorder, yBorder) = limitations;
            var (xPos, yPos) = cursorPosition;
            
            var pressResult = cursorPosition;

            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.UpArrow:
                    pressResult = yPos > 0 ? (xPos, yPos - 1) : cursorPosition;
                    break;
                case ConsoleKey.DownArrow:
                    pressResult = yPos < yBorder ? (xPos, yPos + 1) : cursorPosition;
                    break;
                case ConsoleKey.LeftArrow:
                    pressResult = xPos > 0 ? (xPos - 1, yPos) : cursorPosition;
                    break;
                case ConsoleKey.RightArrow:
                    pressResult = xPos < xBorder ? (xPos + 1, yPos) : cursorPosition;
                    break;
                case ConsoleKey.Enter:
                    return (pressResult.Item1, pressResult.Item2, true);
                
            }
            
            return (pressResult.Item1, pressResult.Item2, false);
        }
        
        
        /// <summary>
        /// Allow User to use navigation system.
        /// </summary>
        /// <param name="cursorPosition">Current cursor position indicator.</param>
        /// <param name="limitations">Border indicators.</param>
        /// <returns>Current cursor position and Success Indicator.</returns>
        public static (int, int, int, int, bool) keyboardControl2((int, int) cursorPosition, (int, int) limitations,
            (int, int) curlen)
        {
            
            // Unpacking Tuples
            var (xBorder, yBorder) = limitations;
            var (xPos, yPos) = cursorPosition;
            
            var xLen = curlen.Item1;

            var yLen = curlen.Item2;
            
            var pressResult = cursorPosition;

            var input = Console.ReadKey();
            
            switch (input.Key, input.Modifiers)
            {
                case (ConsoleKey.UpArrow, 0):
                    pressResult = yPos > 0 ? (xPos, yPos - 1) : cursorPosition;
                    break;
                case (ConsoleKey.DownArrow, 0):
                    pressResult = yPos + yLen - 1 < yBorder ? (xPos, yPos + 1) : cursorPosition;
                    break;
                case (ConsoleKey.LeftArrow, 0):
                    pressResult = xPos > 0 ? (xPos - 1, yPos) : cursorPosition;
                    break;
                case (ConsoleKey.RightArrow, 0):
                    pressResult = xPos + xLen - 1 < xBorder ? (xPos + 1, yPos) : cursorPosition;
                    break;
                case (ConsoleKey.Enter, 0):
                    return (pressResult.Item1, pressResult.Item2, xLen, yLen, true);
                case (ConsoleKey.RightArrow, ConsoleModifiers.Shift):
                    xLen = xLen < xBorder - xPos + 1 ? xLen + 1 : xLen;
                    break;
                case (ConsoleKey.DownArrow, ConsoleModifiers.Shift):
                    yLen = yLen < yBorder - yPos + 1 ? yLen + 1 : yLen;
                    break;
                case (ConsoleKey.LeftArrow, ConsoleModifiers.Shift):
                    xLen = xLen > 1 ? xLen - 1 : xLen;
                    break;
                case (ConsoleKey.UpArrow, ConsoleModifiers.Shift):
                    yLen = yLen > 1 ? yLen - 1 : yLen;
                    break;
            }
            
            return (pressResult.Item1, pressResult.Item2, xLen, yLen, false);
        }
    }
}