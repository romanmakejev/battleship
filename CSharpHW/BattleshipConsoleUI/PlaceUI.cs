﻿using System;
using Domain.Battleship;

using SF  = BattleshipConsoleUI.SharedUI;


namespace BattleshipConsoleUI
{
    
    /// <summary>
    /// Describes BattleShip Game Ship Placing UI Solution.
    /// </summary>
    public class PlaceUI
    {
        
        /// <summary>
        /// Describes current position of cursor on Game Board.
        /// </summary>
        private static (int, int) CursorPosition = (0, 0);

        
        /// <summary>
        /// Describes current Ship X coordinate and Y coordinate sizes..
        /// </summary>
        private static (int, int) ShipSizes = (1, 1);
        
        
        /// <summary>
        /// User Places Ship on the Own Board.
        /// </summary>
        public static (int, int, int, int, string) playerPlaceShip(BoardSquareState[,] board)
        {
            
            var xLenght = board.GetLength(0);
            var yLenght = board.GetLength(1);
            
            var shipName = SF.InputValidation("Choose the Name for the Ship: ", Console.WindowHeight / 2, false);
            
            do
            {
                Console.Clear();
                BoardUI.DrawBoard(board, CursorPosition, ShipSizes);
                
                
                var (xCoord, yCoord, xLen, yLen, isDone) = SF.keyboardControl2(CursorPosition, (xLenght - 1, yLenght - 1), ShipSizes);
                
                CursorPosition = (xCoord, yCoord);
                ShipSizes = (xLen, yLen);
                
                if (isDone) return (CursorPosition.Item1, CursorPosition.Item2, ShipSizes.Item1, ShipSizes.Item2, shipName);

            } while (true);
        }
    }
}