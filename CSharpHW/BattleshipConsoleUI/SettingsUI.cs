﻿using System;
using System.Collections.Generic;
using System.Threading;
using Domain.Menu;
using MenuSystem;
using SF  = BattleshipConsoleUI.SharedUI;


namespace BattleshipConsoleUI
{
    
    /// <summary>
    /// Describes BattleShip Game Settings UI Solution.
    /// </summary>
    public static class SettingsUI
    {
        
        /// <summary>
        /// Game Settings Initialization UI for BattleShip Game instance creation.
        /// </summary>
        public static (int, string, string) SettingsInitialization()
        {
            Console.Clear();
            SF.ScrollBarRemover();
            
            var topPosition = SF.GameHeader();
            SF.GameBoarders(topPosition, Console.WindowHeight - 5);
            SF.GameFooter(Console.WindowHeight - 5);

            SettingMenuInitialization();
                
                
            //
            // Ship Sizes: 10 x 20
            // 
            // Same Ships:  <- ->
            //
            // Can Ships Touch: <- ->
            //
            // Is Single Player: <- ->
            //
            
            
            var boardSize = SF.InputValidation("What is the size of the board? ", 6, true);
            var playerAName = SF.InputValidation("Player A - Choose your Name: ", Console.CursorTop, false);
            var playerBName = SF.InputValidation("Player B - Choose your Name: ", Console.CursorTop, false);
            
            int.TryParse(boardSize, out var convertedBoardSize);
            
            Console.Clear();
            SF.DrawCenteredText("Creating a New Game for You...", -1, Console.WindowHeight / 2, false);
            Thread.Sleep(500);

            return (convertedBoardSize, playerAName, playerBName);
        }

        
        /// <summary>
        /// Menu Initialization for Users to make Settings decision.
        /// </summary>
        /// <returns></returns>
        private static string SettingMenuInitialization()
        {
            
            var gameModeItem = new MenuItem("Game Mode");
            
            gameModeItem.AddMenuItemOptions(new List<MenuItemOption>
            {
                new MenuItemOption("Single Player"),
                new MenuItemOption("Multi Player")
            });
            
            var shipUsageItem = new MenuItem("Ship Usage Mode");
            
            shipUsageItem.AddMenuItemOptions(new List<MenuItemOption>
            {
                new MenuItemOption("Players Use Same Ships."),
                new MenuItemOption("Players Use Own Ships.")
            });
            
            var shipCollisionItem = new MenuItem("Ship Collision Mode");
            
            shipCollisionItem.AddMenuItemOptions(new List<MenuItemOption>
            {
                new MenuItemOption("Standard Collision Mode"),
                new MenuItemOption("Ship Corners Can Touch Mode"),
                new MenuItemOption("Ship All Sides Can Touch Player")
            });
            
            var settingMenu = new Menu("Settings Menu", EMenuLevel.Undefined);
            
            settingMenu.AddMenuItems(new List<MenuItem>
            {
                gameModeItem,
                shipUsageItem,
                shipCollisionItem
            });

            var logic = new MenuLogic(settingMenu);
            logic.Run();
            return "";
        }
    }
}