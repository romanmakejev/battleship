﻿using System;
using System.Threading;

using SF  = BattleshipConsoleUI.SharedUI;


namespace BattleshipConsoleUI
{
    
    /// <summary>
    /// Describes BattleShip Game StartUp UI Solution.
    /// </summary>
    public static class StartUI
    {
        
        /// <summary>
        /// Get Data for creation of an Instance of the BattleShip Game.
        /// </summary>
        public static (int, string, string) gameInitialization()
        {
            Console.Clear();
            
            SF.GameHeader();

            var boardSize = SF.InputValidation("What is the size of the board? ", 6, true);
            var playerAName = SF.InputValidation("Player A - Choose your Name: ", Console.CursorTop, false);
            var playerBName = SF.InputValidation("Player B - Choose your Name: ", Console.CursorTop, false);
            
            int.TryParse(boardSize, out var convertedBoardSize);
            
            Console.Clear();
            SF.DrawCenteredText("Creating a New Game for You...", -1, Console.WindowHeight / 2, false);
            Thread.Sleep(500);

            return (convertedBoardSize, playerAName, playerBName);
        }
    }
}