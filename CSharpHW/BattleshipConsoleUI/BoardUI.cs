﻿using System;
using System.Linq;
using Domain.Battleship;

using SF  = BattleshipConsoleUI.SharedUI;


namespace BattleshipConsoleUI
{
    
    /// <summary>
    /// Describes BattleShip Game Board UI Solution.
    /// </summary>
    public static class BoardUI
    
    {
        /// <summary>
        /// Draw Board on the screen.
        /// </summary>
        /// <param name="board">GameBoard to be drawn.</param>
        /// <param name="selectedCell">Cell which is selected on board.</param>
        /// <param name="selectSize">Sizes of the selection on X and Y coordinate.</param>
        public static void DrawBoard(BoardSquareState[,] board, (int, int) selectedCell, (int, int) selectSize)
        {

            var (xLenght, yLenght, yStringLenght) = BoardSizeLenghtCalculator(board);
            var (xPosition, yPosition) = selectedCell;
            var (xSize, ySize) = selectSize;
            
            SF.GameHeader();
            
            for (var y = 0; y <= yLenght; y++)
            {
                var xSelected = yPosition < y && y <= yPosition + ySize ? xPosition : -1; 
                DrawBoardLine(xLenght, yStringLenght, y, xSelected, xSize, board);
                Console.WriteLine();
            }
        }


        /// <summary>
        /// Draw the header with letters and line with numbers of the board.
        /// </summary>
        /// <param name="xLenght">Size of the x Coordinate on the board.</param>
        /// <param name="yStringLenght">Lenght equivalent in string of the X Coordinate on the board.</param>
        /// <param name="yPosition">Current Position on Y Coordinate.</param>
        /// <param name="xPosition">Current Position on X Coordinate.</param>
        /// <param name="xSize">Size of X coordinate cell selection.</param>
        /// <param name="board">GameBoard to be drawn.</param>
        private static void DrawBoardLine(int xLenght, int yStringLenght, int yPosition, 
                                                                int xPosition, int xSize, BoardSquareState[,] board)
        {
            var reservedSpace = yStringLenght + 3 + xLenght * 2;
            var emptyCellLenght = yStringLenght - yPosition.ToString().Length;

            var stringToDraw = yPosition == 0
                        ? SF.StringRepeater(" ", yStringLenght + 3)
                        : $"{SF.StringRepeater(" ", emptyCellLenght + 1)}{yPosition} |";
            
            SF.DrawCenteredText(stringToDraw, reservedSpace, 5 + yPosition, false);
            
            for (var x = 0; x < xLenght; x++)
            {
                if (yPosition != 0 && (!Enumerable.Range(xPosition, xSize).Contains(x) || xPosition == -1))
                {
                    ColorfulBoardState(board[x, yPosition - 1]);
                    Console.Write("|");
                    continue;
                }

                stringToDraw = yPosition == 0 ? $"{Convert.ToChar(0x0041 + x).ToString()} " : "o|";
                Console.Write(stringToDraw);
            }
        }

        
        /// <summary>
        /// Colorize Board Square based on state.
        /// </summary>
        /// <param name="boardSquareState">State of the current Board Square.</param>
        private static void ColorfulBoardState(BoardSquareState boardSquareState)
        {
            Console.ForegroundColor = (boardSquareState.IsShip, boardSquareState.IsBomb) switch
            {
                (false, true) => ConsoleColor.Blue,
                (true, false) => ConsoleColor.Green,
                (true, true) => ConsoleColor.Red,
                _ => Console.ForegroundColor
            };
            
            Console.Write($"{boardSquareState}");
            
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        
        
        /// <summary>
        /// Calculate Board Size equivalent in String Lenght.
        /// </summary>
        /// <param name="board">GameBoard to calculate equivalent.</param>
        /// <returns>Sizes of the board and String Size lenght.</returns>
        private static (int, int, int) BoardSizeLenghtCalculator(BoardSquareState[,] board)
        {
            var xLenght = board.GetLength(0);
            var yLenght = board.GetLength(1);
            
            return (xLenght, yLenght , yLenght.ToString().Length);
        }
    }
}
