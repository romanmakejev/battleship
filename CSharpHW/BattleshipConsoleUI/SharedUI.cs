﻿using System;
using System.Linq;


namespace BattleshipConsoleUI
{
    
    /// <summary>
    /// Describes Shared Functionality for Battleship Game Solution.
    /// </summary>
    public static class SharedUI
    {

        /// <summary>
        /// Validate User Input on Errors.
        /// </summary>
        /// <param name="message">Message to be displayed for hint.</param>
        /// <param name="xPos">Position on X coordinate for text.</param>
        /// <param name="isNumber">Check if input should be numeric.</param>
        /// <returns></returns>
        public static string InputValidation(string message, int xPos, bool isNumber)
        {
            
            bool isSuccess;
            string inputResult;
            
            do
            {
                DrawCenteredText(message,-1, xPos, false);
                inputResult = Console.ReadLine()?.Trim();
                isSuccess = !isNumber || int.TryParse(inputResult, out _);

            } while (!isSuccess);
            
            return inputResult;
        }

        
        /// <summary>
        /// Draw Header of the Game.
        /// </summary>
        public static void GameHeader()
        {
            DrawCenteredText("-->BattleShip Game 1.0<---", -1, 1, true);
            DrawCenteredText(string.Concat(
                Enumerable.Repeat("=", Console.WindowWidth)), -1, Console.CursorTop + 1, true);
        }

        
        /// <summary>
        /// Repeat the provided text for certain amount of times.
        /// </summary>
        /// <param name="repeatText">Text to be repeated.</param>
        /// <param name="repeatCount">Amount of repetitions.</param>
        /// <returns>Repeated text for certain amount of times.</returns>
        public static string StringRepeater(string repeatText, int repeatCount)
        {
            return string.Concat(Enumerable.Repeat(repeatText, repeatCount));
        }
        
        
        /// <summary>
        /// Draw text in the middle of the console.
        /// </summary>
        /// <param name="textToDraw">String to Draw.</param>
        /// <param name="lenght">Complete Length of the string.</param>
        /// <param name="xPos">Position on X coordinate for text.</param>
        /// <param name="IsNew">Indicator of new line start.</param>
        public static void DrawCenteredText(string textToDraw, int lenght, int xPos, bool IsNew)
        {
            var textLenght = lenght == -1 ? textToDraw.Length : lenght;
            
            Console.SetCursorPosition((Console.WindowWidth - textLenght) / 2 , xPos);
            
            if (IsNew) Console.WriteLine(textToDraw);
            
            else Console.Write(textToDraw);
        }

        
        /// <summary>
        /// Allow User to use navigation system.
        /// </summary>
        /// <param name="cursorPosition">Current cursor position indicator.</param>
        /// <param name="limitations">Border indicators.</param>
        /// <returns>Current cursor position and Success Indicator.</returns>
        public static (int, int, bool) keyboardControl((int, int) cursorPosition, (int, int) limitations)
        {
            
            // Unpacking Tuples
            var (xBorder, yBorder) = limitations;
            var (xPos, yPos) = cursorPosition;
            
            var pressResult = cursorPosition;

            switch (Console.ReadKey().Key)
            {
                case ConsoleKey.UpArrow:
                    pressResult = yPos > 0 ? (xPos, yPos - 1) : cursorPosition;
                    break;
                case ConsoleKey.DownArrow:
                    pressResult = yPos < yBorder ? (xPos, yPos + 1) : cursorPosition;
                    break;
                case ConsoleKey.LeftArrow:
                    pressResult = xPos > 0 ? (xPos - 1, yPos) : cursorPosition;
                    break;
                case ConsoleKey.RightArrow:
                    pressResult = xPos < xBorder ? (xPos + 1, yPos) : cursorPosition;
                    break;
                case ConsoleKey.Enter:
                    return (pressResult.Item1, pressResult.Item2, true);
            }
            
            return (pressResult.Item1, pressResult.Item2, false);
        }
    }
}