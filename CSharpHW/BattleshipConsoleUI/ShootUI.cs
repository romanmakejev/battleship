﻿using System;
using Domain.Battleship;

using SF  = BattleshipConsoleUI.SharedUI;


namespace BattleshipConsoleUI
{
    
    /// <summary>
    /// Describes BattleShip Game Shooting UI Solution.
    /// </summary>
    public static class ShootUI
    {
        
        /// <summary>
        /// Describes current position of cursor on Game Board.
        /// </summary>
        private static (int, int) CursorPosition;
        
        
        /// <summary>
        /// User Makes a Move on the Firing Board.
        /// </summary>
        public static (int, int) playerMakeMove(BoardSquareState[,] board)
        {
            CursorPosition = (0, 0);
            
            var xLenght = board.GetLength(0);
            var yLenght = board.GetLength(1);
            
            do
            {
                Console.Clear();
                BoardUI.DrawBoard(board, CursorPosition, (1, 1));
                
                var (xCoord, yCoord, isDone) = SF.keyboardControl(CursorPosition, (xLenght - 1, yLenght - 1));
                CursorPosition = (xCoord, yCoord);
                
                if (isDone) return CursorPosition;

            } while (true);
        }
    }
}