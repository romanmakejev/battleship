﻿using System;
using System.Linq;
using Domain.Menu;


namespace MenuConsoleUI
{
    
    /// <summary>
    /// Describes Menu Console UI Solution.
    /// </summary>
    public class MenuUI
    {
        
        /// <summary>
        /// Display Menu in Console.
        /// </summary>
        public static void DisplayMenu(Menu menu)
        {
            Console.Clear();
            DrawCenteredText($"====>{menu}<====", 2);
            Console.CursorTop += 2;
            
            
            var menuItems = menu.CopyOfMenuItems();
            var xPos = menu.XCursorPosition;



            foreach (var item in menuItems)
            {

                if (menu.MenuLevel == EMenuLevel.Undefined)
                {
                    Console.Write(item.Title + " -> ");

                    if (xPos == menuItems.IndexOf(item))
                    {
                        ColorDecider(xPos == menuItems.IndexOf(item));
                    }
                    
                    
                    Console.Write(item.CopyOfMenuItemOptions()[item.YCursorPosition]);
                    Console.WriteLine();
                    Console.ResetColor();  
                    
                }
                else
                {
                    ColorDecider(xPos == menuItems.IndexOf(item));
                    DrawCenteredText(item.Title, Console.CursorTop);
                    Console.ResetColor();   
                }
            }
            
            DrawCenteredText(string.Concat(Enumerable.Repeat("=", menu.ToString().Length)), Console.CursorTop + 2);
        }
        
        
        /// <summary>
        /// Activate keyboard control for the Menu.
        /// </summary>
        /// <returns>
        /// Tuple of two elements: first one indicates if Menu is done and second one indicates the results.
        /// </returns>
        public static (bool, string) KeyboardControl(Menu menu)
        {
            var isDone = false;
            var menuItems = menu.CopyOfMenuItems();
            var xPos = menu.XCursorPosition;


            var yPos = menuItems[xPos].YCursorPosition;
            var options = menuItems[xPos].CopyOfMenuItemOptions();
            
            
            
            
            var executionResult = "";
            
            switch (Console.ReadKey(true).Key)
            {
                case ConsoleKey.UpArrow:
                    xPos = xPos > 0 ? xPos - 1 : 0;
                    yPos = menuItems[xPos].YCursorPosition;
                    break;
                case ConsoleKey.DownArrow:
                    xPos = xPos < menuItems.Count - 1 ? xPos + 1 : xPos;
                    yPos = menuItems[xPos].YCursorPosition;
                    break;
                case ConsoleKey.RightArrow:
                    yPos = yPos < options.Count - 1 ? yPos + 1 : yPos;
                    break;
                case ConsoleKey.LeftArrow:
                    yPos = yPos > 0 ? yPos - 1 : 0;
                    break;
                case ConsoleKey.Enter:
                    isDone = menuItems.GetRange(menuItems.Count - menu.ReservedMenuItems, menu.ReservedMenuItems)
                        .Any(x => x == menuItems[xPos]);

                    executionResult = !isDone ? menuItems[xPos].ExecuteMethod() : menuItems[xPos].Title;
                    break;
            }
            
            menu.MoveCursorPosition((xPos, yPos));
            
            return (isDone, executionResult);
        }
        

        /// <summary>
        /// Draw text in the middle of the console.
        /// </summary>
        /// <param name="textToDraw"></param>
        /// <param name="xPos">Position on X Coordinate for text.</param>
        private static void DrawCenteredText(string textToDraw, int xPos)
        {
            Console.SetCursorPosition((Console.WindowWidth - textToDraw.Length) / 2 , xPos);
            Console.WriteLine(textToDraw);
        }

        
        /// <summary>
        /// Choose the color for Menu Item.
        /// </summary>
        /// <param name="isSelected">Indicator of item selection.</param>
        private static void ColorDecider(bool isSelected)
        {
            if (isSelected) Console.ForegroundColor = ConsoleColor.Blue;
        }
    }
}