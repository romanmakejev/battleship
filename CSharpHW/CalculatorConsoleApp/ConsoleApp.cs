﻿using MenuSystem;
using CalculatorSystem;
using System.Collections.Generic;
using System.Globalization;
using Domain.Menu;


namespace ConsoleAppHW01
{
    
    /// <summary>
    /// Console Calculator Application Runtime.
    /// </summary>
    internal static class Program
    {
        
        /// <summary>
        /// Calculator Brain Reference.
        /// </summary>
        private static readonly Calculator _calculator = new();
        
        
        /// <summary>
        /// Calculator Start Up.
        /// </summary>
        private static void Main()
        {
            var mainMenu = new Menu("Calculator Main Menu", EMenuLevel.Root);
            
            mainMenu.AddMenuItems(new List<MenuItem>()
            {
                new( "Binary Operations", SubmenuBinary),
                new( "Unary Operations", SubmenuUnary),
                new( "Initialize Value", _calculator.InitialValue),
                new( "Erase Value", _calculator.Erase)
            });

            new MenuLogic(mainMenu).Run();
        }
        
        
        /// <summary>
        /// Menu of Unary mathematical operations.
        /// </summary>
        /// <returns>String of User Choice.</returns>
        private static string SubmenuUnary()
        {
            var unaryMenu = new Menu("Unary Operations", EMenuLevel.First);
            
            unaryMenu.AddMenuItems(new List<MenuItem>()
            {
                new( "Negate", _calculator.Negate),
                new( "Root", _calculator.Root),
                new( "Square", _calculator.Square),
                new("Abs", _calculator.Abs),
                new("Erase Value", _calculator.Erase)
            });

            return new MenuLogic(unaryMenu).Run();
        }
        
        /// <summary>
        /// Menu of Binary mathematical operations.
        /// </summary>
        /// <returns>String of User Choice.</returns>
        private static string SubmenuBinary()
        {
            var binaryMenu = new Menu("Binary Operations", EMenuLevel.First);
            
            binaryMenu.AddMenuItems(new List<MenuItem>()
            {
                new( "Plus", _calculator.Add),
                new( "Minus", _calculator.Subtract),
                new("Divide", _calculator.Divide),
                new( "Multiply", _calculator.Multiply),
                new( "Elevate", _calculator.Elevate),
                new("Erase Value", _calculator.Erase)
            });
            
            return new MenuLogic(binaryMenu).Run();
        }
    }
}